# Base91 PHP Static Class #

base91 is an advanced method for encoding binary data as ASCII characters. It is similar to base64, but is more efficient and  compact. The overhead produced by base91 depends on the input data. It amounts at most to 23% (versus 33% for base64) and can range down to 14%, which typically occurs on 0-byte blocks. This makes base91 very useful for transferring larger files over binary unsafe connections like e-mail or terminal lines.

###Install with Composer###

```
#!json

{
    "require": {
        "lcherone/base91": "dev-master"
    }
}
```

###Usage###

```
#!php

require 'vendor/autoload.php';

use Base91\Base91;

// Result: 5)GfG?ue$y+/ZQ;mMB
echo $encoded = Base91::encode('example string');

// Result: example string
echo Base91::decode($encoded);
```